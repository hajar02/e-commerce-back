-- Active: DATABASE_CONNECTION


INSERT INTO artist (first_name, last_name, image, description) 
VALUES 
('Alexandre','Noens','url1','Ceci est la description d Alexandre.'),
('Hajar','Zahoui','url2','Ceci est la description d Hajar'),
('Juliette','Suc','url3','Ceci est la description de Juliette');

/* INSERT INTO artist (first_name, last_name, image, description) 
VALUES 
('Alexandre','Noens','url1','Ceci est la description d Alexandre.'); */

INSERT INTO user (first_name, last_name, email, hashed_password, phone_number, street, zipcode, city, role) VALUES 
('Louise','Leblanc','louise.leblanc@gmail.com','oihzlgihrlgozeiul','0189798798','1 rue Victor Hugo','69001','Lyon','ROLE_USER'),
('Caleb','Marel','caleb.marel@laposte.fr','kugiuhylimj','0674298877','35 avenue des frères Lumière','69008','Lyon','ROLE_USER'),
('Camille','Villier','camille.villier@outlook.com','oliyhloujmo','0622975617','82 boulevard des Brotteaux','69006','Lyon','ROLE_USER'),
('Pauline','Truc','pauline.truc@orange.fr','pbyhloujmoulykty','0718962883','35 avenue des frères Lumière','69008','Lyon','ROLE_USER'),
('Mia','Mahoudi','mia.mahoudi@hotmail.fr','aiygkilhluj','0765295467','12 rue Bechevelin','69007','Lyon','ROLE_USER');
/* 
INSERT INTO user (first_name, last_name, email, hashed_password, phone_number, street, zipcode, city, role) VALUES ('Mia','Mahoudi','mia.mahoudi@hotmail.fr','aiygkilhluj','0765295467','12 rue Bechevelin','69007','Lyon','ROLE_USER'); */

INSERT INTO article (artist_id, name, description, image) 
VALUES 
(1, 'noir et blanc N°1', 'description du premier tableau','https://images.unsplash.com/photo-1537884557178-342a575d7d16?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=435&q=80'),
(2, 'Cheval coloré', 'description du deuxième tableau','https://images.unsplash.com/photo-1525278070609-779c7adb7b71?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=847&q=80'),
(3, 'Châlet dans les sapins', 'description du troisième tableau','https://images.unsplash.com/photo-1569091791842-7cfb64e04797?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=435&q=80'),
(1, 'Inconnue', 'description du quatrième tableau','https://images.unsplash.com/photo-1535340582796-799448c62e08?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=464&q=80'),
(2, 'Italia', 'description du cinquième tableau','https://images.unsplash.com/photo-1511854005000-f27912f66ade?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80'),
(3, 'Nothing more', 'description du sixième tableau','https://images.unsplash.com/photo-1505569127510-bde1536937bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80');


/* INSERT INTO article (artist_id, name, description, image) 
VALUES 
(3, 'Nothing more', 'description du sixième tableau','urldusixièmetableau'); */

INSERT INTO comment(article_id,user_id,text, date) VALUES
(1,1,'contenu du premier commentaire','2023-02-26'),
(2,1,'contenu du second commentaire','2023-02-26'),
(3,2,'contenu du troisieme commentaire','2023-02-26'),
(3,3,'contenu du quatrieme commentaire','2023-02-26');

INSERT INTO stock (article_id, size, price, quantity) VALUES 
(1, 'A5', 15, 20),
(1, 'A4', 18.90, 13),
(1, 'A3', 24.30, 32),
(2, 'A5', 15, 18),
(2, 'A4', 18.90, 39),
(2, 'A3', 24.30, 43),
(3, 'A5', 15, 56),
(3, 'A4', 18.90, 92),
(3, 'A3', 24.30, 72),
(4, 'A5', 15, 10),
(4, 'A4', 18.90, 29),
(4, 'A3', 24.30, 59),
(5, 'A5', 15, 2),
(5, 'A4', 18.90, 6),
(5, 'A3', 24.30, 0),
(6, 'A5', 15, 41),
(6, 'A4', 18.90, 13),
(6, 'A3', 24.30, 23);

INSERT INTO `order` (user_id, date, delivery_time, status) VALUES 
(1, '2023-02-03','5 à 10 jours', 'délivrée'),
(2, '2023-02-11','5 à 10 jours', 'en cours'),
(3, '2023-02-14','5 à 10 jours', 'en cours'),
(4, '2023-02-16','5 à 10 jours', 'en cours'),
(5, '2023-02-03','5 à 10 jours', 'délivrée'),
(2, '2023-02-01','5 à 10 jours', 'délivrée'),
(1, '2023-02-03','5 à 10 jours', 'délivrée');

INSERT INTO line_article (line_order_id, stock_id, quantity) VALUES 
(1, 2, 2),
 (1, 4, 1),
 (2, 3, 1);


INSERT INTO user_article(user_id,article_id) VALUES
(1,1),
(2,2),
(2,3);
 
/* SELECT * FROM artist;

SELECT * FROM user;

SELECT * FROM article;

SELECT * FROM comment; */

/* SELECT * FROM order ; */
