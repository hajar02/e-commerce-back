<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230303131538 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE line_article DROP FOREIGN KEY FK_B79E8F017294869C');
        $this->addSql('DROP INDEX IDX_B79E8F017294869C ON line_article');
        $this->addSql('ALTER TABLE line_article ADD stock_id INT DEFAULT NULL, DROP article_id, CHANGE price price DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE line_article ADD CONSTRAINT FK_B79E8F01DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('CREATE INDEX IDX_B79E8F01DCD6110 ON line_article (stock_id)');
        $this->addSql('ALTER TABLE `order` CHANGE total total DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE line_article DROP FOREIGN KEY FK_B79E8F01DCD6110');
        $this->addSql('DROP INDEX IDX_B79E8F01DCD6110 ON line_article');
        $this->addSql('ALTER TABLE line_article ADD article_id INT NOT NULL, DROP stock_id, CHANGE price price DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE line_article ADD CONSTRAINT FK_B79E8F017294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_B79E8F017294869C ON line_article (article_id)');
        $this->addSql('ALTER TABLE `order` CHANGE total total DOUBLE PRECISION NOT NULL');
    }
}
