<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(nullable: true)]
    private ?float $total = null;

    #[ORM\Column(length: 255)]
    private ?string $deliveryTime = null;

    #[ORM\Column(length: 255)]
    private ?string $status = null;

    #[Ignore]
    #[ORM\ManyToOne(inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    // #[Ignore]
    #[ORM\OneToMany(mappedBy: 'lineOrder', fetch:'EAGER', targetEntity: LineArticle::class, orphanRemoval: true)]
    private Collection $lineArticles;

    public function __construct()
    {
        $this->lineArticles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getDeliveryTime(): ?string
    {
        return $this->deliveryTime;
    }

    public function setDeliveryTime(string $deliveryTime): self
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, LineArticle>
     */
    public function getLineArticles(): Collection
    {
        return $this->lineArticles;
    }

    public function addLineArticle(LineArticle $lineArticle): self
    {
        if (!$this->lineArticles->contains($lineArticle)) {
            $this->lineArticles->add($lineArticle);
            $lineArticle->setLineOrder($this);
        }

        return $this;
    }

    public function removeLineArticle(LineArticle $lineArticle): self
    {
        if ($this->lineArticles->removeElement($lineArticle)) {
            // set the owning side to null (unless already changed)
            if ($lineArticle->getLineOrder() === $this) {
                $lineArticle->setLineOrder(null);
            }
        }

        return $this;
    }
}
