<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
class Comment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(length: 600)]
    private ?string $text = null;

    #[Ignore]
    #[ORM\ManyToOne(inversedBy: 'comments', cascade : ['persist'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?Article $article = null;

    // #[Ignore]
    #[ORM\ManyToOne(inversedBy: 'comment', fetch:'EAGER')]
    private ?User $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param int|null $id
     * @param \DateTimeInterface|null $date
     * @param string|null $text

     */
    public function __construct(?int $id,  ?string $text, ?\DateTimeInterface $date,) {
    	$this->id = $id;
    	$this->date = $date;
    	$this->text = $text;
    }
}
