<?php

namespace App\Entity;

use App\Repository\LineArticleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: LineArticleRepository::class)]
class LineArticle
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $quantity = null;

    #[ORM\Column(nullable: true)]
    private ?float $price = null;

    #[ORM\Column(nullable: true)]
    private ?float $star = null;

    #[Ignore]
    #[ORM\ManyToOne(inversedBy: 'lineArticles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Order $lineOrder = null;

    #[Ignore]
    #[ORM\ManyToOne(inversedBy: 'lineArticles', fetch:'EAGER')]
    private ?Stock $stock = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStar(): ?float
    {
        return $this->star;
    }

    public function setStar(?float $star): self
    {
        $this->star = $star;

        return $this;
    }

    public function getLineOrder(): ?Order
    {
        return $this->lineOrder;
    }

    public function setLineOrder(?Order $lineOrder): self
    {
        $this->lineOrder = $lineOrder;

        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    public function setStock(?Stock $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Méthodes qui permettent de récupérer ce dont nous avons besoin, le nom, l'image et cetera ...
     */
    public function getProductName() {
        return $this->stock->getArticle()->getName();
    }

    public function getProductImage() {
        return $this->stock->getArticle()->getImage();
    }

    public function getProductPrice() {
        return $this->stock->getPrice();
    }

    public function getProductSize() {
        return $this->stock->getSize();
    }

    public function getArticleId() {
        return $this->stock->getArticle()->getId();
    }
    
}
