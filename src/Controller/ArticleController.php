<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use App\Entity\Artist;


#[Route('/api/article')]
class ArticleController extends AbstractController
{

    public function __construct(private ArticleRepository $repo)
    {
    }

    #[Route('/articlesByArtist/{artistName}', methods: 'GET')]
    public function articlesByArtist(string $artistName, ArticleRepository $articleRepository): Response
    {
        $articles = $articleRepository->createQueryBuilder('a')
            ->leftJoin('a.artist', 'artist')
            ->where('artist.firstName = :firstName')
            ->setParameter('firstName', $artistName)
            ->getQuery()
            ->getResult();

        return $this->json($articles);
    }


    #[Route('/{id}/artist', methods: 'GET')]
    public function getArticleByArtist(Article $article){
        return $this->json($article->getArtist());
    }

    #[Route('/{id}/stock', methods: 'GET')]
    public function getStock(Article $article){
        return $this->json($article->getStocks());
    }

    /*4 LAST ARTICLES*/
    #[Route('/latest_articles', methods: ['GET'])]
    public function getLatestArticles(ArticleRepository $articleRepository) {
        $latestArticles = $articleRepository->findBy([], ['id' => 'DESC'], 4);
        return $this->json($latestArticles);
    } 

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }
 
    #[Route('/{id}', methods: 'GET')]
    public function one(Article $article){
        return $this->json($article);
    }

    #[Route ('/{artist}', methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, Artist $artist){
        try {
            $article = $serializer->deserialize($request->getContent(), Article::class, 'json');
            $article->setArtist($artist);
            $this->repo->save($article, true);
            return $this->json($article, Response::HTTP_CREATED);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    } 

    #[Route('/{id}', methods: 'DELETE')]
    public function delete(Article $article){
        $this->repo->remove($article, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: ['PATCH', 'PUT'])]
    public function update(Article $article, Request $request, SerializerInterface $serializer){
        try {
            $serializer->deserialize($request->getContent(), Article::class, 'json', [
                'object_to_populate' => $article
            ]);
            $this->repo->save($article, true);
            return $this->json($article);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    

    

    


   
}