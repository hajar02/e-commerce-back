<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\User;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Exception\ValidationFailedException;


#[Route('/api/order')]
class OrderController extends AbstractController
{
    public function __construct(private OrderRepository $repo)
    {
    }

    /**
     * Méthode qui récupère toute les order d'un user via le token de l'user donné dans Auth -> Bearer
     * Et si order il n'y a pas alors on renvoie une response 404
     * 
     * On peux aussi ajouter 'status' => 'en cours' après le ['user' => $this->getUser()
     * pour pouvoir récupérer toutes les order dans le status est en cours
     * ce qui donne :
     * 
     *                $order = $repo->findBy(['user' => $this->getUser(), 'status' => 'en cours']);
     * 
     */
    #[Route('/current', methods: 'GET')]
    public function currentOrder(OrderRepository $repo) {
        $order = $repo->findOneBy(['user' => $this->getUser(), 'status' => 'en cours']);
        if(!$order) {
            return $this->json(['error' => 'No Current Order'], Response::HTTP_NOT_FOUND);
        }

        return $this->json($order);
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Order $order){
        return $this->json($order);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer){
        try {
            $order = new Order;
            $order = $serializer->deserialize($request->getContent(), Order::class, 'json');

            $order->setUser($this->getUser());
            $this->repo->save($order, true);

            return $this->json($order, Response::HTTP_CREATED);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function delete(Order $order){
        $this->repo->remove($order, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: ['PATCH', 'PUT'])]
    public function update(Order $order, Request $request, SerializerInterface $serializer){
        try {
            $serializer->deserialize($request->getContent(), Order::class, 'json', [
                'object_to_populate' => $order
            ]);
            $this->repo->save($order, true);
            return $this->json($order);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

}