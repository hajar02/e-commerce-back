<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Exception\ValidationFailedException;


#[Route('/api/comment')]
class CommentController extends AbstractController
{
    public function __construct(private CommentRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Comment $comment){
        return $this->json($comment);
    }

    //ROUTE POUR AFFICHER LES COMMENTS POUR CHAQUE ARTICLE
    #[Route('/article/{article_id}', methods: ['GET'])]
    public function commentsByArticle(int $article_id,  CommentRepository $commentRepository){
        $comments = $commentRepository->commentsByArticle($article_id);
        return $this->json($comments);
    }

    #[Route('/{article}', methods: 'POST')]
    public function add(Article $article, Request $request, SerializerInterface $serializer){
        try {

            $comment = new Comment;
            $comment = $serializer->deserialize($request->getContent(), Comment::class, 'json');

            $comment->setArticle($article);
            $comment->setUser($this->getUser());
            $comment->setDate(new \DateTime());

            $this->repo->save($comment, true);
            return $this->json($comment, Response::HTTP_CREATED);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function delete(Comment $comment){
        $this->repo->remove($comment, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: ['PATCH', 'PUT'])]
    public function update(Comment $comment, Request $request, SerializerInterface $serializer){
        try {
            $serializer->deserialize($request->getContent(), Comment::class, 'json', [
                'object_to_populate' => $comment
            ]);
            $this->repo->save($comment, true);
            return $this->json($comment);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

}