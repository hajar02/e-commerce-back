<?php

namespace App\Controller;

use App\Entity\Stock;
use App\Entity\Article;
use App\Repository\StockRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Exception\ValidationFailedException;


#[Route('/api/stock')]
class StockController extends AbstractController
{
    public function __construct(private StockRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Stock $stock){
        return $this->json($stock);
    }

    #[Route('/{article}', methods: 'POST')]
    public function add(Article $article, Request $request, SerializerInterface $serializer){
        try {
            $stock=new Stock;
            $stock = $serializer->deserialize($request->getContent(), Stock::class, 'json');
            $stock->setArticle($article);

            $this->repo->save($stock, true);
            return $this->json($stock, Response::HTTP_CREATED);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function delete(Stock $stock){
        $this->repo->remove($stock, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: ['PATCH', 'PUT'])]
    public function update(Stock $stock, Request $request, SerializerInterface $serializer){
        try {
            $serializer->deserialize($request->getContent(), Stock::class, 'json', [
                'object_to_populate' => $stock
            ]);
            $this->repo->save($stock, true);
            return $this->json($stock);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}/article', methods: 'GET')]
    public function byArticle(Article $article){

        $stock = $this->repo->findBy(["article" => $article]);

        return $this->json($stock);
    }

}