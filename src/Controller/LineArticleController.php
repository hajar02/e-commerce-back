<?php

namespace App\Controller;

use App\Entity\LineArticle;
use App\Entity\Order;
use App\Entity\Stock;
use App\Repository\LineArticleRepository;
use App\Repository\OrderRepository;
use App\Repository\StockRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Exception\ValidationFailedException;


#[Route('/api/lineArticle')]
class LineArticleController extends AbstractController
{
    public function __construct(private LineArticleRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(LineArticle $lineArticle){
        return $this->json($lineArticle);
    }

    #[Route('/{order}/{stock}', methods: 'POST')]
    public function add(Order $order, Stock $stock,Request $request, SerializerInterface $serializer){
        try {
            $lineArticle = new LineArticle;
            $lineArticle = $serializer->deserialize($request->getContent(), LineArticle::class, 'json');
            $lineArticle->setStock($stock);
            $lineArticle->setLineOrder($order);
            $lineArticle->setPrice($stock->getPrice()*$lineArticle->getQuantity());
            $this->repo->save($lineArticle, true);
            return $this->json($lineArticle, Response::HTTP_CREATED);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function delete(LineArticle $lineArticle){
        $this->repo->remove($lineArticle, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: ['PATCH', 'PUT'])]
    public function update(StockRepository $stockRepo, LineArticle $lineArticle, Request $request, SerializerInterface $serializer){
        try {
            $serializer->deserialize($request->getContent(), LineArticle::class, 'json', [
                'object_to_populate' => $lineArticle
            ]);
        // $request->toArray()['stock'];
        // $stock = $repo->findBy($request->toArray()['stock']);

        // $lineArticle->setStock($stockRepo->findBy($request->toArray()['stock']));

            $this->repo->save($lineArticle, true);
            return $this->json($lineArticle);
        }catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

}